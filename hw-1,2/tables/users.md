# USERS

Таблица с сущностью пользователь. Пользователи регистрируются на сайте по email(логин). После регистрации им приходит
письмо на указанный при регистрации email и переходя по ссылке их письма они подтверждают email. У пользователя может
быть несколько ролей. Пользователь без роли может читать и писать посты, комментарии, ставить лайки/дизлайки постам и
комментариям, получать уведомления, добавлять статьи в закладки. Пользователи с ролью модератор может модерировать
пользователей
(блокировать/разблокировать) и модерировать комментарии (удалять/восстанавливать). Пользователи с ролью корректор может
корректировать посты пользователей. У каждого пользователя есть рейтинг, который растет или уменьшается в зависимости от
полезности его статей и комментариев.

## FIELDS

|Column          |Type        |Nullable|Default|Description                                       |
|----------------|------------|--------|-------|--------------------------------------------------|
|id              |serial      |NOT NULL|       |id записи                              |
|email           |varchar(191)|NOT NULL|       |email пользователя                                |
|name            |varchar(191)|NOT NULL|       |Имя пользователя                                  |
|surname         |varchar(191)|NOT NULL|       |Фамилия пользователя                              |
|password        |varchar(191)|NOT NULL|       |Пароль                                            |
|nick            |varchar(50) |NULL    |       |Ник пользователя                                  |
|createdAt       |timestamp   |NOT NULL|       |Timestamp создания                                |
|updatedAt       |timestamp   |NOT NULL|       |Timestamp последнего редактирования               |
|emailConfirmedAt|timestamp   |NOT NULL|       |Timestamp подтверждения email                     |
|moderatedAt     |timestamp   |NULL    |       |Timestamp модерации                               |
|moderatedBy     |timestamp   |NULL    |       |Id модератора, который промодерировал пользователя|
|deletedAt       |timestamp   |NULL    |       |Timestamp удаления                                |
|rating          |integer     |NOT NULL|0      |Рейтинг пользователя                              |

## CONSTRAINTS

### CHECK

| Name              | Check                                                    | 
|-------------------|----------------------------------------------------------| 
| users-email-check | email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$' | 
| users-nick-check  | (char_length(name) > 3)                                  |

### KEYS

|Name                |Type       |Columns    |References|RefTable|RefColumn|
|--------------------|-----------|-----------|----------|--------|---------|
|users-pk            |PRIMARY KEY|id         |          |        |         |
|users-moderatedBy-fk|FOREIGN KEY|moderatedBy|REFERENCES|users   |id       |

## INDEXES

| Index name        | Unique | Columns     | Description                                                                          | 
|-------------------|--------|-------------|--------------------------------------------------------------------------------------| 
| users-pkey        | yes    | id          | Нужен для поиска по id.                                                              | 
| users-moderatedBy | no     | moderatedBy | Нужен для поиска постов, которые опубликовал конкретный модератор                    | 
| users-email       | yes    | email       | Нужен для поиска по email. Поиск при авторизации, поиск email пользователя в админке | 
| users-nick        | yes    | nick        | Нужен для поиска пользователя по нику                                                | 

