# THEMES_POSTS

Таблица связи тема-пост.

## FIELDS

| Column    | Type      | Nullable | Default           | Description        | 
|-----------|-----------|----------|-------------------|--------------------| 
| postId    | integer   | NOT NULL |                   | id поста           | 
| themeId   | integer   | NOT NULL |                   | id темы            | 
| createdAt | timestamp | NOT NULL | CURRENT TIMESTAMP | Timestamp создания | 

## CONSTRAINTS

### KEYS

| Name                    | Type        | Columns         | References | RefTable | RefColumn | 
|-------------------------|-------------|-----------------|------------|----------|-----------| 
| themes_posts-pk         | PRIMARY KEY | postId, themeId |            |          |           | 
| themes_posts-postId-pk  | FOREIGN KEY | postId          | REFERENCES | posts    | id        | 
| themes_posts-themeId-fk | FOREIGN KEY | themeId         | REFERENCES | themes   | id        |

## INDEXES

| Index name           | Unique | Columns         | Description                                       | 
|----------------------|--------|-----------------|---------------------------------------------------| 
| themes_posts-pkey    | yes    | postId, themeId | Уникальный первичный ключ, для связи постов и тем | 
| themes_posts-postId  | no     | postId          | Нужен для поиска тем для постов                   | 
| themes_posts-themeId | no     | themeId         | Нужен для постов по темам                         | 

