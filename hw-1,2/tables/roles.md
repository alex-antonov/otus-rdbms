# ROLES

Справочник ролей пользователя.

## FIELDS

|Column     |Type        |Nullable|Default|Description         |
|-----------|------------|--------|-------|--------------------|
|id         |serial      |NOT NULL|       |id роли             |
|name       |varchar(50) |NOT NULL|       |Имя роли            |
|description|varchar(191)|NOT NULL|       |Описание роли       |

## CONSTRAINTS

### KEYS

|Name       |Type        |Columns |References|RefTable            |RefColumn|
|-----------|------------|--------|----------|--------------------|---------|
|roles-pk   |PRIMARY KEY |id      |          |                    |         |

## INDEXES

| Index name | Unique | Columns | Description            | 
|------------|--------|---------|------------------------| 
| roles-pkey | yes    | id      | Нужен для поиска по id | 

