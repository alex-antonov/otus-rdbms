# SEO

Сущность содержащая сео данные. В данной таблице реализованна полиморфная связь с темами и постами.

## FIELDS

| Column      | Type         | Nullable | Default           | Description                                         |
|-------------|--------------|----------|-------------------|-----------------------------------------------------|
| id          | serial       | NOT NULL |                   | Id сео данных                                       |
| entityId    | integer      | NOT NULL |                   | Id сущности                                         |
| entityType  | varchar(50)  | NOT NULL |                   | Тип сущности                                        |
| keywords    | text         | NOT NULL |                   | Ключевые слова                                      |
| description | text         | NOT NULL |                   | Описание                                            |
| title       | varchar(191) | NOT NULL |                   | Заголовок                                           |
| createdAt   | timestamp    | NOT NULL | CURRENT TIMESTAMP | Timestamp создания                                  |
| updatedAt   | timestamp    | NULL     |                   | Timestamp редактирования                            |
| createdBy   | integer      | NOT NULL |                   | Id пользователя, который создал сео блок            |
| updatedBy   | integer      | NULL     |                   | Id пользователя, который последний обновил сео блок |

## CONSTRAINTS

### KEYS

| Name             | Type        | Columns   | References | RefTable | RefColumn | 
|------------------|-------------|-----------|------------|----------|-----------| 
| seo-pk           | PRIMARY KEY | id        |            |          |           | 
| seo-createdBy-fk | FOREIGN KEY | createdBy | REFERENCES | users    | id        | 
| seo-updatedBy-fk | FOREIGN KEY | updatedBy | REFERENCES | users    | id        | 

## INDEXES

| Index name              | Unique | Columns              | Description                                                                                        | 
|-------------------------|--------|----------------------|----------------------------------------------------------------------------------------------------| 
| seo-pkey                | yes    | id                   | Нужен для поиска по id                                                                             | 
| seo-entityId-entityType | yes    | entityId, entityType | Уникальный индекс для поиска по entityId-entityType. По данным полям реализуется полиморфная связь |

