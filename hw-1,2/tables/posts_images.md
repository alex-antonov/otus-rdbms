# POSTS_IMAGES

Картинки поста. У поста может быть несколько картинок. Но только одна главная. Главная картинка выводиться в ленте всех
постов.

## FIELDS

| Column    | Type      | Nullable | Default | Description                          | 
|-----------|-----------|----------|---------|--------------------------------------| 
| id        | bigserial | NOT NULL |         | id картинки поста                    | 
| postId    | integer   | NOT NULL |         | id поста                             | 
| isMain    | boolean   | NOT NULL | FALSE   | Является ли картинка главной у поста | 
| updatedAt | timestamp | NULL     |         | Timestamp редактирования             | 

## CONSTRAINTS

### KEYS

| Name                  | Type        | Columns | References | RefTable | RefColumn | 
|-----------------------|-------------|---------|------------|----------|-----------| 
| post_images-pk        |             |         |            |          |           | 
| post_images-postId-fk | FOREIGN KEY | postId  | REFERENCES | posts    | id        |

## INDEXES

| Index name            | Unique | Columns | Description               | 
|-----------------------|--------|---------|---------------------------| 
| post_images-pkey      | yes    | id      | Нужен для поиска по id    | 
| post_images-postId-fk | no     | postId  | Нужен для поиска по посту |
 