# BOOKMARKS

Сущность закладки поста.

## FIELDS

| Column    | Type      | Nullable | Default           | Description        |
|-----------|-----------|----------|-------------------|--------------------|
| postId    | integer   | NOT NULL |                   | Id поста           |
| userId    | integer   | NOT NULL |                   | Id пользователя    |
| createdAt | timestamp | NOT NULL | CURRENT TIMESTAMP | Timestamp создания |

## CONSTRAINTS

### KEYS

| Name                | Type        | Columns | References | RefTable | RefColumn | 
|---------------------|-------------|---------|------------|----------|-----------| 
| bookmarks-pk        | PRIMARY KEY | id      |            |          |           | 
| bookmarks-userId-fk | FOREIGN KEY | userId  | REFERENCES | users    | id        | 
| bookmarks-postId-fk | FOREIGN KEY | postId  | REFERENCES | posts    | id        |

## INDEXES

| Index name              | Unique | Columns        | Description                                                                                                      | 
|-------------------------|--------|----------------|------------------------------------------------------------------------------------------------------------------| 
| bookmarks-pkey          | yes    | id             | Нужен для поиска по id                                                                                           | 
| bookmarks-userId        | no     | userId         | Нужен для поиска закладок по пользователю                                                                        | 
| bookmarks-postId        | no     | postId         | Нужен для поиска закладок по посту. Например для подсчета сколько пользователь добавили данную статью в закладки | 
| bookmarks-userId-postId | yes    | userId, postId | Уникальный индекс для исключения дубликатов                                                                       | 


 