#THEMES
Сущность темы(категории постов).

##FIELDS

| Column      | Type         | Nullable | Default           | Description              |
|-------------|--------------|----------|-------------------|--------------------------|
| id          | serial       | NOT NULL |                   | id темы                  |
| description | varchar(191) | NOT NULL |                   | Описание темы            |
| createdAt   | timestamp    | NOT NULL | CURRENT TIMESTAMP | Timestamp создания       |
| updatedAt   | timestamp    | NULLABLE |                   | Timestamp редактирования |
| slug        | varchar(191) | NOT NULL |                   | slug поста               |


##CONSTRAINTS

###KEYS

| Name      | Type        | Columns | References | RefTable | RefColumn |
|-----------|-------------|---------|------------|----------|-----------|
| themes-pk | PRIMARY KEY | id      |            |          |           |

##INDEXES

| Index name                 | Unique | Columns              | Description                                                                                        | 
|----------------------------|--------|----------------------|----------------------------------------------------------------------------------------------------| 
| images-pkey                | yes    | id                   | Нужен для поиска по id                                                                             |
