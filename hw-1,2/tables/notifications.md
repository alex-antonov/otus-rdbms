# NOTIFICATIONS

Сущность уведомления. Так как предполагается, что типов уведомлений будет много, данные самого уведомления будут
храниться в jsonb - поле data. Например, уведомление об ответе на комментарий, уведомление о новом комментарии статьи,
лайки, или уведомления от модераторов(модерация поста, профиля и т.д)

## FIELDS

| Column    | Type        | Nullable | Default           | Description                                 | 
|-----------|-------------|----------|-------------------|---------------------------------------------| 
| id        | bigserial   | NOT NULL |                   | id уведомления                              | 
| type      | varchar(50) | NOT NULL |                   | Тип уведомления                             | 
| data      | jsonb       | NOT NULL |                   | Данные уведомления                          | 
| isRead    | boolean     | NULL     | FALSE             | Флаг, показывающий прочитано ли уведомление | 
| userId    | integer     | NOT NULL |                   | Id пользователя                             | 
| createdAt | timestamp   | NOT NULL | CURRENT TIMESTAMP | Timestamp создания                          | 

## CONSTRAINTS

### KEYS

| Name                    | Type        | Columns     | References | RefTable | RefColumn | 
|-------------------------|-------------|-------------|------------|----------|-----------| 
| notifications-pk        | PRIMARY KEY | id          |            |          |           | 
| notifications-userId-fk | FOREIGN KEY | userId      | REFERENCES | users    | id        | 

## INDEXES

| Index name                  | Unique | Columns   | Description                                                                                   | 
|-----------------------------|--------|-----------|-----------------------------------------------------------------------------------------------| 
| notifications-pkey          | yes    | id        | Нужен для поиска по id                                                                        | 
| notifications-userId        | no     | userId    | Нужен для поиска уведомлений по пользователю                                                  | 
| notifications-createdAt-idx | no     | createdAt | Нужен для поиска статей по дате создания. Например для показа сначала самых новых уведомлений | 




 