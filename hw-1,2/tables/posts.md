# POSTS

Таблица с сущностью пост(статья), которую пишут пользователи, ставят лайки/дизлайки. Также пользователи с привилегией
модератор могут публиковать или снимать с публикации пост.

## FIELDS

| Column                | Type         | Nullable | Default           | Description                                                  | 
|-----------------------|--------------|----------|-------------------|--------------------------------------------------------------| 
| id                    | serial       | NOT NULL |                   | id записи                                                    | 
| authorId              | integer      | NOT NULL |                   | id автора                                                    | 
| text                  | text         | NOT NULL |                   | Текст поста                                                  | 
| createdAt             | timestamp    | NOT NULL | CURRENT TIMESTAMP | Timestamp создания                                           | 
| updatedAt             | timestamp    | NULL     |                   | Timestamp редактирования                                     | 
| header                | varchar(191) | NOT NULL |                   | Заголовок                                                    | 
| summary               | varchar(191) | NOT NULL |                   | Краткое содержание                                           | 
| publishedAt           | timestamp    | NULL     |                   | Timestamp публикации                                         | 
| removedAt             | timestamp    | NULL     |                   | Timestamp снятия с публикации                                | 
| isCommentable         | boolean      | NOT NULL | TRUE              | Флаг определяющий, можно ли оставлять комментарии под постом | 
| publishedBy           | integer      | NULL     |                   | id модератора, который опубликовал пост                      | 
| removedBy             | integer      | NULL     |                   | id модератора, который отклонил пост                         | 
| likesCount            | integer      | NOT NULL | 0                 | Количество лайков поста                                      | 
| dislikesCount         | integer      | NOT NULL | 0                 | Количество дизлайков поста                                   | 
| viewsCount            | integer      | NOT NULL | 0                 | Количество просмотров поста                                  | 
| slug                  | varchar(191) | NOT NULL |                   | slug поста                                                   |

## CONSTRAINTS

### KEYS

| Constraint name      | Type        | Columns     | References | RefTable | RefColumn | 
|----------------------|-------------|-------------|------------|----------|-----------| 
| posts-pk             | PRIMARY KEY | id          |            |          |           | 
| posts-authorId-fk    | FOREIGN KEY | authorId    | REFERENCES | users    | id        | 
| posts-publishedBy-fk | FOREIGN KEY | publishedBy | REFERENCES | users    | id        | 
| posts-removedBy-fk   | FOREIGN KEY | removedBy   | REFERENCES | users    | id        | 

## INDEXES

| Index name            | Unique | Columns     | Description                                                     | 
|-----------------------|--------|-------------|-----------------------------------------------------------------| 
| posts-idx             | yes    | id          | Нужен для поиска по id                                          | 
| posts-authorId-idx    | no     | authorId    | Нужен для поиска какие статьи написал определенный пользователь | 
| posts-publishedBy-idx | no     | publishedBy | Нужен для какие посты отклонил определенный модератор           | 
| posts-removedBy-idx   | no     | removedBy   | Нужен для поиска какие посты отклонил определенный модератор    | 
| posts-publishedAt-idx | no     | publishedAt | Нужен для поиска постов по дате публикации                      | 

 