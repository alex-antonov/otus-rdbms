# IMAGES

Сущность картинки. Реализованна полиморфная связь с аватарками пользователей и картинками новостей. Хранит пути к
расположению оригинального файла и картинок разных расширений(large, medium, small).

## FIELDS

| Column         | Type         | Nullable | Default           | Description                            | 
|----------------|--------------|----------|-------------------|----------------------------------------| 
| id             | serial       | NOT NULL |                   | id картинки                            | 
| entityType     | varchar(50)  | NOT NULL |                   | Тип картинки                           | 
| mimeType       | varchar(50)  | NOT NULL |                   | Mime type файла                        | 
| originalPath   | varchar(191) | NOT NULL |                   | Путь до оригинального файла            | 
| largeSizePath  | varchar(191) | NULL     |                   | Путь до файла большого размера         | 
| mediumSizePath | varchar(191) | NULL     |                   | Путь до файла среднего размера         | 
| smallSizePath  | varchar(191) | NULL     |                   | Путь до файла малого размера           | 
| uploadedBy     | integer      | NOT NULL |                   | id пользователя, загрузившего картинку | 
| uploadedAt     | timestamp    | NOT NULL | CURRENT TIMESTAMP | Timestamp загрузки картинки            | 

## CONSTRAINTS

| Name                 | Type        | Columns    | References | RefTable | RefColumn | 
|----------------------|-------------|------------|------------|----------|-----------| 
| images-pk            | PRIMARY KEY | id         |            |          |           | 
| images-uploadedBy-fk | FOREIGN KEY | uploadedBy | REFERENCES | users    | id        |

## INDEXES

| Index name                 | Unique | Columns              | 
|----------------------------|--------|----------------------| 
| images-pkey                | yes    | id                   | 
| images-entityId-entityType | yes    | entityId, entityType | 


 