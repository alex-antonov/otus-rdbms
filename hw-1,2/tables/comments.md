# COMMENTS

Сущность комментарии. Комментарии оставляют авторизованные пользователи, также пользователи ставят лайки/дизлайки
комментариям. Комментарии могут быть вложенными.

## FIELDS

| Column       | Type        | Nullable | Default           | Description                              | 
|--------------|-------------|----------|-------------------|------------------------------------------| 
| id           | bigserial   | NOT NULL |                   | Id комментария                           | 
| text         | text        | NOT NULL |                   | Текст комментария                        | 
| createdAt    | timestamp   | NOT NULL | CURRENT TIMESTAMP | Timestamp создания                       | 
| updatedAt    | timestamp   | NULL     |                   | Timestamp редактирования                 | 
| deletedAt    | timestamp   | NULL     |                   | Timestamp удаления                       | 
| likeCount    | int         | NOT NULL | 0                 | Количество лайков                        | 
| dislikeCount | int         | NOT NULL | 0                 | Количество дизлайков                     | 
| userId       | integer     | NOT NULL |                   | Id пользователя, написавшего комментарий | 
| postId       | integer     | NOT NULL |                   | Id поста, на который написан комментарий | 
| parentId     | biginteger  | NULL     |                   | Родительский комментарий                 | 

## CONSTRAINTS

### KEYS

| Name                 | Type        | Columns  | References | RefTable | RefColumn | 
|----------------------|-------------|----------|------------|----------|-----------| 
| comments-pk          | PRIMARY KEY | id       |            |          |           | 
| comments-postId-fk   | FOREIGN KEY | postId   | REFERENCES | posts    | id        | 
| comments-userId-fk   | FOREIGN KEY | userId   | REFERENCES | users    | id        | 
| comments-parentId-fk | FOREIGN KEY | comments | REFERENCES | comments | id        |

## INDEXES

| Index name        | Unique | Columns  | Description                                                            | 
|-------------------|--------|----------|------------------------------------------------------------------------| 
| comments-pkey     | yes    | id       | Нужен для поиска по id                                                 | 
| comments-postId   | no     | postId   | Нужен для поиска комментариев по посту                                 | 
| comments-userId   | no     | userId   | Нужен для поиска комментариев по пользователю, написавшему комментарий | 
| comments-parentId | no     | comments | Нужен для поиска родительского комментария                             | 
