# AVATARS

Сущность аватара — картинка в профиле. У пользователя может быть несколько аватаров, но активным может быть только один.

## FIELDS

| Column      | Type      | Nullable | Default           | Description                                                        | 
|-------------|-----------|----------|-------------------|--------------------------------------------------------------------| 
| id          | serial    | NOT NULL |                   | id аватара                                                         | 
| userId      | integer   | NOT NULL |                   | id пользователя                                                    | 
| isActive    | boolean   | NOT NULL | FALSE             | Флаг, показывающий является ли это аватар активным в данный момент | 
| createdAt   | timestamp | NOT NULL | CURRENT TIMESTAMP | Timestamp создания                                                 | 
| updatedAt   | timestamp | NULL     |                   | Timestamp редактирования                                           | 
| validatedAt | timestamp | NULL     |                   | Timestamp прохождения валидации                                    |

## CONSTRAINTS

### KEYS

| Name              | Type        | Columns | References | RefTable | RefColumn | 
|-------------------|-------------|---------|------------|----------|-----------| 
| avatars-pk        | PRIMARY KEY | id      |            |          |           | 
| avatars-userId-fk | FOREIGN KEY | userId  | REFERENCES | users    | id        |

## INDEXES

| Index name     | Unique | Columns | Description                      | 
|----------------|--------|---------|----------------------------------| 
| avatars-pkey   | yes    | id      | Нужен для поиска по id           | 
| avatars-userId | no     | userId  | Нужен для поиска по пользователю | 


 