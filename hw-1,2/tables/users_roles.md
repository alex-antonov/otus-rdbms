#USERS_ROLES

Таблица связи пользователь-роль.

##FIELDS

|Column   |Type     |Nullable|Default          |Description       |
|---------|---------|--------|-----------------|------------------|
|userId   |integer  |NOT NULL|                 |Id пользователя   |
|roleId   |integer  |NOT NULL|                 |Id роли           |
|createdAt|timestamp|NOT NULL|CURRENT TIMESTAMP|Timestamp создания|

##CONSTRAINTS

###KEYS

|Name                 |Type       |Columns       |References|RefTable|RefColumn|
|---------------------|-----------|--------------|----------|--------|---------|
|users_roles-pk       |PRIMARY KEY|userId, roleId|          |        |         |
|users_roles-userId-fk|FOREIGN KEY|userId        |REFERENCES|users   |id       |
|users_roles-roleId-fk|FOREIGN KEY|roleId        |REFERENCES|roles   |id       |

##INDEXES

| Index name         | Unique | Columns        | Description                                                                                                                | 
|--------------------|--------|----------------|----------------------------------------------------------------------------------------------------------------------------| 
| users_roles-pkey   | yes    | userId, roleId | Уникальный первичный ключ, для связи пользователей и ролей                                                                 | 
| users_roles-userId | no     | userId         | Нужен для поиска ролей по пользователям                                                                                    | 
| users_roles-roleId | no     | roleId         | Нужен для поиска пользователей по ролям. Например в админке посмотреть каеие(сколько) пользовтелей имеют определенную роль | 


 